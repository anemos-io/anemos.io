PROJECTS=makeimg secman archlinux-ec2

PROJECT_TARGETS = $(foreach PROJECT,$(PROJECTS),target/projects/$(PROJECT)/index.html)

all: $(PROJECT_TARGETS) target/4xx.html

deploy:
	scp -r target/* anemos.io:anemos/

target/4xx.html: 4xx.html templates/page_header.html templates/page_footer.html
	m4 -DTITLE="Not a thing - anemos.io" < templates/page_header.html > $@
	cat $< >> $@
	m4 < templates/page_footer.html >> $@

target/projects/%/index.html: projects/%/index.html templates/page_header.html templates/page_footer.html
	mkdir -p $(@D)
	m4 -DTITLE=$* < templates/page_header.html > $@
	cat projects/$*/index.html >> $@
	m4 < templates/page_footer.html >> $@

clean:
	rm -f $(PROJECT_TARGETS) target/4xx.html

.PHONY: all deploy clean
